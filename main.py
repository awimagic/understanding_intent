import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
#formatter = logging.Formatter('%(asctime)s - %(name)-8s - %(levelname)-8s - %(module)-8s:%(funcName)-8s:%(lineno)-3d - %(message)s')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(message)s')
# console handler
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)
# file handler
fh = logging.FileHandler('intent.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)


df = pd.read_csv('data.csv')

categories = df['intent'].unique()
reversed_categories = dict(zip(list(range(len(categories))), categories))
categories = dict(zip(categories, list(range(len(categories)))))

df['categories'] = df['intent'].apply(lambda x: categories[x])


v1 = TfidfVectorizer(ngram_range=(1, 3))
t1 = v1.fit_transform(df['command'])

v2 = TfidfVectorizer(stop_words='english', ngram_range=(1, 3))
t2 = v2.fit_transform(df['command'])


from sklearn.naive_bayes import MultinomialNB
clf = MultinomialNB().fit(t1, df['categories'])

userinput = 'list files'
while userinput:
    userinput = input('Enter command (e.g. "list files"): ')
    logger.info('userinput: %s', userinput)
    p('awhan')
    if not userinput:
        print('bye!')
        break
    u = v1.transform([userinput])
    p = clf.predict(u)
    logger.info('predicted intent: %s', reversed_categories[p[0]])

