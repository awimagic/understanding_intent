import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import logging
from flask import Flask
from flask import render_template


app = Flask(__name__)


df = pd.read_csv('data.csv')
categories = df['intent'].unique()
reversed_categories = dict(zip(list(range(len(categories))), categories))
categories = dict(zip(categories, list(range(len(categories)))))

df['categories'] = df['intent'].apply(lambda x: categories[x])


v1 = TfidfVectorizer(ngram_range=(1, 3))
t1 = v1.fit_transform(df['command'])

v2 = TfidfVectorizer(stop_words='english', ngram_range=(1, 3))
t2 = v2.fit_transform(df['command'])


from sklearn.naive_bayes import MultinomialNB
clf = MultinomialNB().fit(t1, df['categories'])


# userinput = 'list files'
# while userinput:
#     userinput = input('Enter command (e.g. "list files"): ')
#     if not userinput:
#         print('bye!')
#         break
#     u = v1.transform([userinput])
#     p = clf.predict(u)
#     print(reversed_categories[p[0]])

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        pass
    else:
        return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)    
